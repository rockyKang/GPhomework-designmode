package com.rockkang.gp.homework.designmode.factory.simple;

import com.rockkang.gp.homework.designmode.factory.pojo.GoodsFoods;
import com.rockkang.gp.homework.designmode.factory.pojo.Goods;
import com.rockkang.gp.homework.designmode.factory.pojo.GoodsTicket;

public class SimpleFactory {
    public Goods getGoods(String goodsName) {
        if ("foods".equals(goodsName)) {
            return new GoodsFoods();
        } else if ("ticket".equals(goodsName)){
            return new GoodsTicket();
        }
        return null;
    }
}
