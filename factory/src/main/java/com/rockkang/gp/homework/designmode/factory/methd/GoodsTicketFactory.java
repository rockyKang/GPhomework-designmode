package com.rockkang.gp.homework.designmode.factory.methd;

import com.rockkang.gp.homework.designmode.factory.pojo.Goods;
import com.rockkang.gp.homework.designmode.factory.pojo.GoodsTicket;

public class GoodsTicketFactory implements GoodsFactory {
    @Override
    public Goods createGoods() {
        return new GoodsTicket();
    }
}
