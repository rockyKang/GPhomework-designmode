package com.rockkang.gp.homework.designmode.factory.methd;

import com.rockkang.gp.homework.designmode.factory.pojo.Warehouse;

public interface WarehouseFactory {
    Warehouse createWarehouse();
}
