package com.rockkang.gp.homework.designmode.factory.test;

import com.rockkang.gp.homework.designmode.factory.abstractfactory.DeliveryFactory;
import com.rockkang.gp.homework.designmode.factory.abstractfactory.PDDeliveryFactory;
import com.rockkang.gp.homework.designmode.factory.pojo.Customer;
import com.rockkang.gp.homework.designmode.factory.pojo.Express;
import com.rockkang.gp.homework.designmode.factory.pojo.Warehouse;

public class AbstractFactoryTest {
    public static void main(String[] arms) {
        // 消费者购买货物，经由消费者所在地区仓库出货，委派给指定地区快递点，送至消费者手中。
        /*
        pojo： 1.消费者，2.仓库，3，快递点
        methodFactory, abstractFactory
         */
        DeliveryFactory deliveryFactory = new PDDeliveryFactory();
        Customer customer = deliveryFactory.createCustomer();
        Express express = deliveryFactory.createExpress();
        Warehouse warehouse = deliveryFactory.createWarehouse();
        System.out.println(warehouse);
        System.out.println(express);
        System.out.println(customer);

    }
}
