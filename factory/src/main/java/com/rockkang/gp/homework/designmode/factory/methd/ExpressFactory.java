package com.rockkang.gp.homework.designmode.factory.methd;

import com.rockkang.gp.homework.designmode.factory.pojo.Express;

public interface ExpressFactory {
    Express createExpress();
}
