package com.rockkang.gp.homework.designmode.factory.abstractfactory;

import com.rockkang.gp.homework.designmode.factory.pojo.*;

public class PDDeliveryFactory implements DeliveryFactory {

    @Override
    public Customer createCustomer() {
        return new CustomerBePD();
    }

    @Override
    public Express createExpress() {
        return new ExpressBePD();
    }

    @Override
    public Warehouse createWarehouse() {
        return new WarehouseBePD();
    }
}
