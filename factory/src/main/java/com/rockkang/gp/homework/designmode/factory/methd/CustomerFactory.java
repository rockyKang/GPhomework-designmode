package com.rockkang.gp.homework.designmode.factory.methd;

import com.rockkang.gp.homework.designmode.factory.pojo.Customer;

public interface CustomerFactory {
    Customer createCustomer();
}
