package com.rockkang.gp.homework.designmode.factory.methd;

import com.rockkang.gp.homework.designmode.factory.pojo.GoodsFoods;
import com.rockkang.gp.homework.designmode.factory.pojo.Goods;

public class GoodsFoodsFactory implements GoodsFactory {
    @Override
    public Goods createGoods() {
        return new GoodsFoods();
    }
}
