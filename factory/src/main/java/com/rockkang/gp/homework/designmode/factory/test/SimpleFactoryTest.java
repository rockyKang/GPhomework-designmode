package com.rockkang.gp.homework.designmode.factory.test;

import com.rockkang.gp.homework.designmode.factory.pojo.Goods;
import com.rockkang.gp.homework.designmode.factory.simple.SimpleFactory;

public class SimpleFactoryTest {
    public static void main (String[] args) {
        SimpleFactory sf = new SimpleFactory();
        Goods goodsForFoods = sf.getGoods("foods");
        Goods goodsForTicket = sf.getGoods("ticket");
        System.out.println(goodsForFoods);
        System.out.println(goodsForTicket);
    }

}
