package com.rockkang.gp.homework.designmode.factory.methd;

import com.rockkang.gp.homework.designmode.factory.pojo.Goods;

public interface GoodsFactory {
    Goods createGoods();
}
