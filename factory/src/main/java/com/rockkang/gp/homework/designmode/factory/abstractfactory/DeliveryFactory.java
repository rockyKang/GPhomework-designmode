package com.rockkang.gp.homework.designmode.factory.abstractfactory;

import com.rockkang.gp.homework.designmode.factory.methd.CustomerFactory;
import com.rockkang.gp.homework.designmode.factory.methd.ExpressFactory;
import com.rockkang.gp.homework.designmode.factory.methd.WarehouseFactory;

public interface DeliveryFactory extends CustomerFactory, ExpressFactory, WarehouseFactory {
}
