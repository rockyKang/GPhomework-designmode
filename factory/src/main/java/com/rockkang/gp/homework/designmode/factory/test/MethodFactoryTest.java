package com.rockkang.gp.homework.designmode.factory.test;

import com.rockkang.gp.homework.designmode.factory.methd.GoodsFoodsFactory;
import com.rockkang.gp.homework.designmode.factory.methd.GoodsFactory;
import com.rockkang.gp.homework.designmode.factory.methd.GoodsTicketFactory;

public class MethodFactoryTest {
    public static void main(String[] arms) {
        GoodsFactory foodsFactory = new GoodsFoodsFactory();
        GoodsFactory ticketFactory = new GoodsTicketFactory();
        System.out.println(foodsFactory.createGoods());
        System.out.println(ticketFactory.createGoods());

    }
}
