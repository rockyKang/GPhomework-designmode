# 咕泡作业-设计模式-工厂模式

#### 介绍
用于提交设计模式 工厂模式作业

#### 软件架构
1.简单工厂方法。
  简单，无代码依赖关系。 扩展困难，需改变原有逻辑
2.工厂方法
  扩展方便，只需建立工厂实现类。缺点不易管理
3.抽象工厂
  优点，为使用者建立约束，可读性好。方便扩展同流程不同产品。不易需改流程属性。


#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)