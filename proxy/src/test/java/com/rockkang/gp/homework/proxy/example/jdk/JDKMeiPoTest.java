package com.rockkang.gp.homework.proxy.example.jdk;

import com.rockkang.gp.homework.proxy.example.entity.Businessman;
import com.rockkang.gp.homework.proxy.example.entity.Person;
import org.junit.Test;

public class JDKMeiPoTest {

    @Test
    public void invoke() {
        Person pPerson = new Businessman("name");
        Person person =  new JDKMeiPo().getInstance(pPerson);
        System.out.println("-------------------test-invoke--------------------");
        System.out.println(person instanceof Businessman);
        System.out.println(person.getClass());
        System.out.println(pPerson);
        System.out.println(person);
        person.findLove();
        System.out.println("-------------show hello-------------");
    }
}