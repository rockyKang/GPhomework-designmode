package com.rockkang.gp.homework.proxy.example;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.SimpleTimeZone;

public class OrderServiceStaticProxyTest {

    @Test
    public void createOrder() throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2019, 2, 23);
        System.out.println(calendar.getTime());
        System.out.println(new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime()));
        Order order = new Order();
        order.setCreateTime(calendar.getTimeInMillis());
        OrderService orderService = new OrderServiceStaticProxy(new OrderServiceImpl());
        orderService.createOrder(order);
    }
}