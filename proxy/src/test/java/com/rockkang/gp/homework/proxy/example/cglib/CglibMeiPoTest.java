package com.rockkang.gp.homework.proxy.example.cglib;

import com.rockkang.gp.homework.proxy.example.entity.Businessman;
import com.rockkang.gp.homework.proxy.example.entity.Person;
import net.sf.cglib.proxy.Factory;
import org.junit.Test;

public class CglibMeiPoTest {

    @Test
    public void getInstance() {
        Person person = (Person) CglibMeiPo.getInstance(Businessman.class);
        Person personByName = (Person) CglibMeiPo.getInstance(Businessman.class, "贩二");
//        Class personClazz = person.getClass();
//        Class personByNameClazz = person.getClass();
//        System.out.println(personClazz);
//        System.out.println(personByNameClazz);
//        System.out.println(personClazz.getSuperclass());
//        System.out.println(personByNameClazz.getSuperclass());
//        System.out.println("===========toString============");
//        System.out.println(person);
//        System.out.println("===========toString end============");
//        System.out.println("===========equals============");
//        System.out.println(person.equals("abc"));
//        System.out.println("===========equals end============");
        person.findLove();
        personByName.findLove();
    }
}