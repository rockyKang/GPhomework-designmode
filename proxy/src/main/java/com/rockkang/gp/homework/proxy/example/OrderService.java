package com.rockkang.gp.homework.proxy.example;

public interface OrderService {
    int createOrder(Order order);
}
