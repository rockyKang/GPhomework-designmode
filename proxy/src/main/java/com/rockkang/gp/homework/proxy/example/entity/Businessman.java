package com.rockkang.gp.homework.proxy.example.entity;

public class Businessman extends BaseMan implements Person {
    private String name;

    public Businessman() {
//        if (true) {
//            throw new RuntimeException("businessman Constructor构造方法");
//        }
        super();
    }

    public Businessman(String name) {
        this.name = name;
    }

    @Override
    public void findLove() {
        System.out.println("businessman:"+ (name==null?"I":name) +" want to find lover");
//        if (true) {
//            throw new RuntimeException("businessman findLove");
//        }
    }

//    @Override
//    public void findLove() {
//        super.findLove();
//    }

//    @Override
//    public String toString() {
//        System.out.println("------------businessman-toString--------------------");
//        return super.toString();
//    }

    public void showHello() {
        System.out.println("show hello");
    }
}
