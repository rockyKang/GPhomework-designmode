package com.rockkang.gp.homework.proxy.example.entity;

public class BaseMan implements Person{
    @Override
    public void findLove() {
        System.out.println("baseMan findLove");
    }
}
