package com.rockkang.gp.homework.proxy.simple;

public class Courier {
    private Person person;

    public Courier(Person person) {
        this.person = person;
    }

    public void goShopping(){
        System.out.println("---------------挑选商铺挑选产片-------------------");
        this.person.goShopping();
        System.out.println("---------------交易完成------------------");
    }

    public static void main(String[] args) {
        Courier courier = new Courier(new Customer());
        courier.goShopping();
    }
}
