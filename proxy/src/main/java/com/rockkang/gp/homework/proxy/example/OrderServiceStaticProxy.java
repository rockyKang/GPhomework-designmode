package com.rockkang.gp.homework.proxy.example;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class OrderServiceStaticProxy implements OrderService {

    private SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");

    private OrderService orderService;

    public OrderServiceStaticProxy(OrderService orderService) {
        this.orderService = orderService;
    }

    @Override
    public int createOrder(Order order) {
        this.before(order);
        orderService.createOrder(order);
        this.after(order);
        return 0;
    }

    private void before(Order order){
        System.out.println("Proxy before method");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(order.getCreateTime());
        System.out.println("静态代理类自动分配【DB_"  + calendar.get(Calendar.YEAR) +  "】数据源。");
        DynamicDataSourceEntry.set(calendar.get(Calendar.YEAR));
    }

    private void after(Order order){
        System.out.println("Proxy after method");
        System.out.println("静态代理类自动分配去除分配数据源");
        DynamicDataSourceEntry.restore();
    }
}
