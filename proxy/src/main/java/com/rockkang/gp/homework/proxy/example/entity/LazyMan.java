package com.rockkang.gp.homework.proxy.example.entity;

public class LazyMan implements Person {
    @Override
    public void findLove() {
        System.out.println("----------I am too lazy to find lover. you need to help me-------------------");
    }
}
