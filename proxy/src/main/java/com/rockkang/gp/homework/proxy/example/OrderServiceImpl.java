package com.rockkang.gp.homework.proxy.example;

public class OrderServiceImpl implements OrderService {
    private OrderDao orderDao;

    public OrderServiceImpl() {
        this.orderDao = new OrderDao();
    }

    @Override
    public int createOrder(Order order) {
        System.out.println("使用orderDao 调用insert方法插入order");
        this.orderDao.insert(order);
        return 0;
    }
}
