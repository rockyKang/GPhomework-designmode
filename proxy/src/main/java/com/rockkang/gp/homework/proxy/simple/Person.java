package com.rockkang.gp.homework.proxy.simple;

public interface Person {
    void goShopping();
}
