package com.rockkang.gp.homework.proxy.example.jdk;

import com.rockkang.gp.homework.proxy.example.entity.Person;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class JDKMeiPo implements InvocationHandler {

    private Person target;

    public  Person getInstance(Person person) {
        this.target = person;
        Class<? extends Person> clazz = person.getClass();
        return (Person) Proxy.newProxyInstance(clazz.getClassLoader(), clazz.getInterfaces(), this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("-------------JDKMeiPo invoke------------------");
        before(proxy, method, args);
//        Object object = method.invoke(proxy, args);
        Object object = method.invoke(this.target, args);
        after(proxy, method, args);
        return object;
    }

    private void after(Object proxy, Method method, Object[] args) {
        System.out.println("动态代理after方法 method:" + method.getName());
    }

    private void before(Object proxy, Method method, Object[] args) {
        System.out.println("动态代理before方法 method:" + method.getName());
        System.out.println("--------------JDKMeiPo invoke before start------------------------------");
//        System.out.println(proxy);
        System.out.println(proxy.getClass());
        System.out.println("--------------JDKMeiPo invoke before end------------------------------");
    }

}
