package com.rockkang.gp.homework.pattern.prototype.test;

import com.rockkang.gp.homework.designmode.prototype.PrototypeData;
import com.rockkang.gp.homework.designmode.prototype.PrototypeV2;

public class V2Test {
    public static void main(String[] args) {
        PrototypeV2 prototypeV2 = new PrototypeV2();
        prototypeV2.setAge(123);
        prototypeV2.setData(new PrototypeData("a_data"));
        prototypeV2.setName("prototype_A");

        PrototypeV2 prototype = (PrototypeV2)prototypeV2.clone();
        System.out.println(prototype);
        System.out.println(prototypeV2);
        System.out.println(prototype.getData() == prototypeV2.getData());
    }
}
