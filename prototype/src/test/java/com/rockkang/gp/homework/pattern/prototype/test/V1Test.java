package com.rockkang.gp.homework.pattern.prototype.test;

import com.rockkang.gp.homework.designmode.prototype.Client;
import com.rockkang.gp.homework.designmode.prototype.PrototypeV1;

import java.util.ArrayList;

public class V1Test {
    public static void main(String[] args) {
        Client<PrototypeV1> client = new Client();
        PrototypeV1 prototype1 = new PrototypeV1();
        prototype1.setName("张三");
        prototype1.setAge(123);
        prototype1.setHobbies(new ArrayList());
        PrototypeV1 prototype2 = (PrototypeV1) prototype1.clone();
        PrototypeV1 prototype3 = client.startClone(prototype1);
        System.out.println(prototype1);
        System.out.println(prototype2);
        System.out.println(prototype3);
        System.out.println(prototype1.getHobbies() == prototype2.getHobbies());
    }
}
