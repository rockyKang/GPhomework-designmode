package com.rockkang.gp.homework.pattern.prototype.test;

import com.rockkang.gp.homework.designmode.prototype.PrototypeJdk;
import com.rockkang.gp.homework.designmode.prototype.PrototypeV1;

import java.util.ArrayList;

public class JdkTest {
    public static void main(String[] args) {
        PrototypeJdk prototype1 = new PrototypeJdk();
        prototype1.setName("张三");
        prototype1.setAge(123);
        prototype1.setHobbies(new ArrayList());

        PrototypeJdk prototype2 = (PrototypeJdk) prototype1.clone();
        System.out.println(prototype1);
        System.out.println(prototype2);
        if (prototype2 != null) System.out.println(prototype1.getHobbies() == prototype2.getHobbies());
    }
}
