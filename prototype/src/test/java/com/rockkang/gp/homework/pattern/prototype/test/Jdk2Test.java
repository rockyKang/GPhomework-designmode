package com.rockkang.gp.homework.pattern.prototype.test;

import com.rockkang.gp.homework.designmode.prototype.PrototypeData;
import com.rockkang.gp.homework.designmode.prototype.PrototypeJdk2;

public class Jdk2Test {
    public static void main(String[] args) {
        PrototypeJdk2 prototype1 = new PrototypeJdk2();
        prototype1.setName("张三");
        prototype1.setAge(123);
        prototype1.setData(new PrototypeData("name_a"));
        PrototypeJdk2 prototype2 = (PrototypeJdk2) prototype1.clone();
        System.out.println(prototype1);
        System.out.println(prototype2);
        System.out.println(prototype1.getData());
        System.out.println(prototype2.getData());
        System.out.println(prototype1.getData() == prototype2.getData());
    }
}
