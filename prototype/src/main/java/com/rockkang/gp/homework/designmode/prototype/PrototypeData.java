package com.rockkang.gp.homework.designmode.prototype;

import java.io.Serializable;
import java.util.Date;

public class PrototypeData implements Serializable {
    private Date date;
    private String name;

    public PrototypeData() {
        System.out.println("-------------PrototypeData--------------");
        this.date = new Date();
    }

    public PrototypeData(String name) {
        this();
        this.name = name;
        System.out.println("-------------PrototypeData-name-------------");
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
