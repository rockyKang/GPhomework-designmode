package com.rockkang.gp.homework.designmode.prototype;

public interface Prototype {
    Prototype clone();
}
