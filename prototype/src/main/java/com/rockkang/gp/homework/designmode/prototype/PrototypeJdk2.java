package com.rockkang.gp.homework.designmode.prototype;

public class PrototypeJdk2 implements Cloneable {
    private String name;
    private Integer age;
    private PrototypeData data;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public PrototypeData getData() {
        return data;
    }

    public void setData(PrototypeData data) {
        this.data = data;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
}
