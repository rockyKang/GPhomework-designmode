package com.rockkang.gp.homework.designmode.prototype;

import java.io.*;

public class PrototypeV2 implements Cloneable, Serializable{
    private String name;
    private Integer age;
    private PrototypeData data;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public PrototypeData getData() {
        return data;
    }

    public void setData(PrototypeData data) {
        this.data = data;
    }

    @Override
    public Object clone() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(this);
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            return ois.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
