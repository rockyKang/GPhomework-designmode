package com.rockkang.gp.homework.designmode.prototype;

import java.util.List;

public class PrototypeV1 implements Prototype{
    private String name;
    private Integer age;
    private List hobbies;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public List getHobbies() {
        return hobbies;
    }

    public void setHobbies(List hobbies) {
        this.hobbies = hobbies;
    }

    @Override
    public Prototype clone() {
        PrototypeV1 prototypeV1 = new PrototypeV1();
        prototypeV1.setAge(this.age);
        prototypeV1.setName(this.name);
        prototypeV1.setHobbies(this.hobbies);
        return prototypeV1;
    }
}
