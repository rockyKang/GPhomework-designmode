package com.rockkang.gp.homework.designmode.prototype;

public class Client<T extends Prototype>{

    public T startClone(T prototype) {
        return (T) prototype.clone();
    }
}
