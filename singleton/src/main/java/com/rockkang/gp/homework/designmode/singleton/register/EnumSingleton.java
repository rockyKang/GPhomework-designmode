package com.rockkang.gp.homework.designmode.singleton.register;

import java.io.Serializable;

public enum  EnumSingleton implements Serializable {
    INSTANCE;

    private Object data;
    EnumSingleton() {
        System.out.println("--------EnumSingleton-构造方法------------------");
        System.out.println("--------ordinal-"+ this.ordinal() +"------------------------------------");
    }
    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public static EnumSingleton getInstance() {
        return INSTANCE;
    }

    public static void sayHelloWorld() {
        System.out.println("----------EnumSingleton-say-hello-to-world-------------------");
    }
}
