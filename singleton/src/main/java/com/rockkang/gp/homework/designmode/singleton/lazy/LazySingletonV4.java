package com.rockkang.gp.homework.designmode.singleton.lazy;

import java.io.Serializable;

/**
 * 3.inner class 内部类
 */
public class LazySingletonV4 implements Serializable {
    private static final long serialVersionUID = -8997873429652781764L;

    static {
        System.out.println("--------------LazySingletonV4-static------------------");
    }
    private LazySingletonV4() {
        System.out.println("----------LazySingletonV4-constructor--------------------------");
        if (LazySingletonV4Holder.lazySingletonV4 != null) {
            throw new RuntimeException("不允许创建多个实例");
        }
    }

    /**
     * 控制台打印HelloWorld
     */
    public static void showHelloWorld() {
        /*
         * 证明不是内部类使用时加载
         */
        System.out.println("------LazySingletonV4-hello-world----------------------------");
    }

    public static LazySingletonV4 getInstance(){
        /*
         * 利用内部列使用时加载的特性实现懒加载
         */
        return LazySingletonV4Holder.lazySingletonV4;
    }

    private static class LazySingletonV4Holder {
        static {
            System.out.println("----------------LazySingletonV4Holder-static---------------------");
        }
        private static final LazySingletonV4 lazySingletonV4 = new LazySingletonV4();
    }

    /**
     * 反序列化时调用方法
     *   反序列化时，依然会创建构造读取到对象。只是弃之不用而已。
     *   需要查看反序列化时调用此方法的过程，再此方法中抛出一个已成根据异常信息可以清晰查看调用路径
     *
     * @return 返回反序列化获取对象
     */
    public Object readResolve(){
        if (true) {
            throw new RuntimeException("查看反序列化调用过程");
        }
        return getInstance();
    }
}
