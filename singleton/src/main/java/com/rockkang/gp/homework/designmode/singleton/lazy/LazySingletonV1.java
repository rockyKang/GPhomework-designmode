package com.rockkang.gp.homework.designmode.singleton.lazy;

/**
 * 1.简单懒汉式单例
 */
public class LazySingletonV1 {
    private static LazySingletonV1 LAZY_SINGLETON_V1;

    private LazySingletonV1() {}

    public static LazySingletonV1 getInstance(){
        System.out.println("------------LazySingletonV1-getInstance--------------");
        /**
         * 并发访问可获得多个实例
         */
        if (LAZY_SINGLETON_V1 == null) {
            LAZY_SINGLETON_V1 =  new LazySingletonV1();
        }
        return LAZY_SINGLETON_V1;
    }
}
