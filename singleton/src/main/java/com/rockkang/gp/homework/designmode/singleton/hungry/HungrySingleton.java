package com.rockkang.gp.homework.designmode.singleton.hungry;

public class HungrySingleton {
    private static final HungrySingleton HUNGRY_SINGLETON = new HungrySingleton();

    private HungrySingleton() {
        if (HUNGRY_SINGLETON != null) {
            throw new RuntimeException("单例不可重复初始化");
        }
    }

    public static HungrySingleton getInstance() {
        return HUNGRY_SINGLETON;
    }


}
