package com.rockkang.gp.homework.designmode.singleton.lazy;

/**
 * 3.double check
 */
public class LazySingletonV3 {
    private static LazySingletonV3 LAZY_SINGLETON_V2;

    private LazySingletonV3() {}

    public static LazySingletonV3 getInstance(){
        /**
         * 当LAZY_SINGLETON_V2不为空时跳过线程锁部分节省性能
         */
        if (LAZY_SINGLETON_V2 == null) {
            synchronized (LazySingletonV3.class) {
                if (LAZY_SINGLETON_V2 == null) {
                    LAZY_SINGLETON_V2 =  new LazySingletonV3();
                }
            }
        }
        return LAZY_SINGLETON_V2;
    }

}
