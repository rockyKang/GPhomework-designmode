package com.rockkang.gp.homework.designmode.singleton.ienum;

public enum Temp {
    /**
     * 调用显示注释A
     */
    A,

    /**
     * 调用显示注释B
     */
    B
}
