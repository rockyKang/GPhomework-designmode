package com.rockkang.gp.homework.designmode.singleton.lazy;

/**
 * 2.线程锁synchronized
 */
public class LazySingletonV2 {
    private static LazySingletonV2 LAZY_SINGLETON_V2;

    private LazySingletonV2() {}

    public static synchronized LazySingletonV2 getInstance(){
        /**
         * 线程锁可防止并发，缺点是同时只有一个线程访问此method。严重影响性能
         */
        if (LAZY_SINGLETON_V2 == null) {
            LAZY_SINGLETON_V2 =  new LazySingletonV2();
        }
        return LAZY_SINGLETON_V2;
    }

    public static void helloWorld(){
        /**
         * 通过验证synchronized不会影响非synchronized修饰的方法
         */
        System.out.println("-------------hello world-----------------");
    }
}
