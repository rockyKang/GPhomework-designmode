package com.rockkang.gp.homework.designmode.singleton.excepiton;

public class BeanInvalidException extends RuntimeException {
    public BeanInvalidException() {
    }

    public BeanInvalidException(String message) {
        super(message);
    }

    public BeanInvalidException(String message, Throwable cause) {
        super(message, cause);
    }

    public BeanInvalidException(Throwable cause) {
        super(cause);
    }

    public BeanInvalidException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
