package com.rockkang.gp.homework.designmode.singleton.utils;

import java.io.File;
import java.io.IOException;

public class FileUtil {
    public static File getFile(String path) throws IOException {
        File file = new File(path);
        if (!file.exists()) {
            File parent = file.getParentFile();
            if (!parent.exists()) {
                parent.mkdirs();
            }
            file.createNewFile();
        }
        return file;
    }
}
