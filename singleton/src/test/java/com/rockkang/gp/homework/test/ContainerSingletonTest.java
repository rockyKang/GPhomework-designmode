package com.rockkang.gp.homework.test;

import com.rockkang.gp.homework.designmode.singleton.lazy.LazySingletonV1;
import com.rockkang.gp.homework.designmode.singleton.register.ContainerSingleton;

public class ContainerSingletonTest {
    public static void main(String[] arms) {
        String clazzName = LazySingletonV1.class.getName();
        Object obj = ContainerSingleton.getBean(clazzName);
        System.out.println(clazzName);
        System.out.println(obj);
//        try {
//            LazySingletonV1 lazySingletonV1 = LazySingletonV1.class.newInstance();
//        } catch (InstantiationException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//            throw new BeanInvalidException(e);
//        }
    }
}
