package com.rockkang.gp.homework.test;

import com.rockkang.gp.homework.designmode.singleton.register.EnumSingleton;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class EnumSingletonTest {
    public static void main(String[] arms) throws IOException {
        // 演示初始化时间start
//        System.out.println("------------main------------------");
//        EnumSingleton.sayHelloWorld();
//        LazySingletonV4.showHelloWorld();
//        LazySingletonV4.getInstance();
//        LazySingletonV4.showHelloWorld();
//        System.out.println("--------------EnumSingleton-start----------------------");
//        EnumSingleton es = EnumSingleton.INSTANCE;
        // 演示初始化时间end
        // 查看构造方法start
//        es.setData(LazySingletonV4.getInstance());
//        System.out.println(es.getData());
//        System.out.println(EnumSingleton.INSTANCE.getData());
//        Constructor[] constructorsArr = EnumSingleton.class.getConstructors();
//        System.out.println(constructorsArr.length);
//        try {
//            Constructor<EnumSingleton> constructor = EnumSingleton.class.getConstructor();
//            System.out.println(constructor);
//
//        } catch (NoSuchMethodException e) {
//            e.printStackTrace();
//        }
        // 查看构造方法end
        try {
            Constructor<EnumSingleton> constructor = EnumSingleton.class.getDeclaredConstructor(String.class, int.class);
            constructor.setAccessible(true);
            constructor.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        // 序列化反序列化破坏单例start
//        EnumSingleton originalEnumSingleton = EnumSingleton.INSTANCE;
//        System.out.println(originalEnumSingleton);
//        File enumSingletonFile =  FileUtil.getFile("C:\\Users\\xuekang\\Desktop\\wdit\\temp\\io/originalEnumSingleton.obj");
//        try {
//            ObjectOutput oo = new ObjectOutputStream(new FileOutputStream(enumSingletonFile));
//            oo.writeObject(originalEnumSingleton);
//            oo.flush();
//            oo.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        try {
//            ObjectInput oi = new ObjectInputStream(new FileInputStream(enumSingletonFile));
//            EnumSingleton readEnumSingleTon = (EnumSingleton) oi.readObject();
//            oi.close();
//            readEnumSingleTon.setData("hello world");
//            System.out.println(readEnumSingleTon);
//            System.out.println(originalEnumSingleton == readEnumSingleTon); // 句柄比较为true
//            // 使用readEnumSingleton引用设置data为"hello world" originalEnumSingleton同时改变证明为引用同一对象
//            System.out.println(originalEnumSingleton.getData());
//            System.out.println(readEnumSingleTon.getData());
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
        // 序列化反序列化破坏单例end
    }
}
