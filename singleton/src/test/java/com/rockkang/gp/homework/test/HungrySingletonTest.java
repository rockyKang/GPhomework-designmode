package com.rockkang.gp.homework.test;

import com.rockkang.gp.homework.designmode.singleton.hungry.HungrySingleton;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class HungrySingletonTest {
    public static void main(String[] arms) throws IOException, ClassNotFoundException {
        HungrySingleton.getInstance();
        File f = new File("temp");
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f));
        Object o = ois.readObject();
    }
}
