package com.rockkang.gp.homework.test;

import com.rockkang.gp.homework.designmode.singleton.lazy.LazySingletonV4;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectBreakSingleton {
    public static void main(String[] arms) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class<LazySingletonV4> clazz = LazySingletonV4.class;
        Constructor<LazySingletonV4> constructor = (Constructor<LazySingletonV4>)clazz.getDeclaredConstructor(null);
        constructor.setAccessible(true);
        LazySingletonV4 lazy1 = null;
        LazySingletonV4 lazy2 = null;
        LazySingletonV4 lazy3 = null;
        try {
            lazy1 = (LazySingletonV4) constructor.newInstance();
        } catch (Exception e) {
//            e.printStackTrace();
        }
        try {
            lazy2 = (LazySingletonV4) constructor.newInstance();
        } catch (Exception e) {
//            e.printStackTrace();
        }
        try {
            lazy3 = LazySingletonV4.getInstance();
        } catch (Exception e) {
//            e.printStackTrace();
        }
        System.out.println(lazy1);
        System.out.println(lazy2);
        System.out.println(lazy3);

        Method method = clazz.getMethod("showHelloWorld");
        method.invoke(null);
    }
}
