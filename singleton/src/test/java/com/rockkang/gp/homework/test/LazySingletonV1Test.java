package com.rockkang.gp.homework.test;

import com.rockkang.gp.homework.designmode.singleton.lazy.LazySingletonV1;

public class LazySingletonV1Test {
    public static void main(String[] arms) {
        Thread thread1 = getSingletonThread();
        Thread thread2 = getSingletonThread();

        thread1.start();
        thread2.start();

        System.out.println("--------------main end--------------");
    }

    public static Thread getSingletonThread() {
        return new Thread(new Runnable() {
            @Override
            public void run() {

                Thread t = Thread.currentThread();
                System.out.println("-------------" + t.getName() + ":start----------------------");
                LazySingletonV1 lazySingletonV1 = LazySingletonV1.getInstance();
                System.out.println(lazySingletonV1);
            }
        });
    }
}
