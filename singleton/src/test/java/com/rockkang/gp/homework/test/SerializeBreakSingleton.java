package com.rockkang.gp.homework.test;

import com.rockkang.gp.homework.designmode.singleton.lazy.LazySingletonV4;
import com.rockkang.gp.homework.designmode.singleton.utils.FileUtil;

import java.io.*;

public class SerializeBreakSingleton {
    public static void main(String[] arms) throws IOException {
        LazySingletonV4 lazySingletonV4 = LazySingletonV4.getInstance();

//        File directoryFile = new File("C:\\Users\\xuekang\\Desktop\\wdit\\temp\\io" );
//        if (!directoryFile.exists()) {
//            if (directoryFile.mkdirs()) {
//                System.out.println("创建文件失败");
//            }
//        }
//        File lazySingletonV4File = new File(directoryFile, "lazySingletonV4.obj");
        File lazySingletonV4File = FileUtil.getFile("C:\\Users\\xuekang\\Desktop\\wdit\\temp\\io/lazySingletonV4.obj");
        ObjectOutput oo = null;
        ObjectInput oi = null;
        try {
            oo = new ObjectOutputStream(new FileOutputStream(lazySingletonV4File));
            oo.writeObject(lazySingletonV4);
            oo.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            oi = new ObjectInputStream(new FileInputStream(lazySingletonV4File));
//            LazySingletonV4 lazy2 = (LazySingletonV4) oi.readObject();
            System.out.println(lazySingletonV4);
            System.out.println(oi.readObject());
        }catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }  finally {
            if (oo != null) {
                try {
                    oo.close();
                } catch (IOException e) {
//                    e.printStackTrace();
                }
            }
            if (oi != null) {
                try {
                    oi.close();
                } catch (IOException e) {
//                    e.printStackTrace();
                }
            }
        }
    }
}
