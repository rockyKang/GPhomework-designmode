package com.rockkang.gp.homework.test;

import java.io.File;
import java.io.IOException;


public class TempTest {
    public static void main(String[] arms) {
        // 1.单例种类 饿汉式，懒汉式，注册式，ThreadLocal

        /*
        1.饿汉式
            优点:没有加任何的锁、执行效率比较高，在用户体验上来说，比懒汉式更好。
            缺点:类加载的时候就初始化，不管用与不用都占着空间，浪费了内存，有可能占着茅坑不拉屎。
        2.懒汉式

         */
//        Thread t1 = LazySingletonV2Test.getSingletonThread();
//        Thread t2 = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("-----LazySingletonV2-helloWorld-------------");
//                LazySingletonV2.helloWorld();
//            }
//        });
//
//        t1.start();
//        t2.start();
//        System.out.println(LazySingletonV4.getInstance());
//        Temp t = Temp.A;
//        LazySingletonV4.sayHelloWorld();
//        try {
//            LazySingletonV4 lazySingletonV4 = LazySingletonV4.getInstance();
//            Method method = LazySingletonV4.class.getMethod("readResolve");
//            Object obj = method.invoke(lazySingletonV4, null);
//            System.out.println(obj.getClass().getName());
//            System.out.println(obj);
//            System.out.println(lazySingletonV4);
//        } catch (NoSuchMethodException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        } catch (InvocationTargetException e) {
//            e.printStackTrace();
//        }
        // 创建新文件（无父类文件夹）
        File file = new File("C:\\Users\\xuekang\\Desktop\\wdit\\temp\\io/temp/fun/temp.file");
        if (!file.exists()) {
            File parentFile = file.getParentFile();
            if (!parentFile.exists()) {
                parentFile.mkdirs();
            }
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
