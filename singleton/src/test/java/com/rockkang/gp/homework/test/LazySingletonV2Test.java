package com.rockkang.gp.homework.test;

import com.rockkang.gp.homework.designmode.singleton.lazy.LazySingletonV2;

public class LazySingletonV2Test {
    public static void main(String[] arms) {
        Thread thread1 = getSingletonThread();
        Thread thread2 = getSingletonThread();

        thread1.start();
        thread2.start();

        System.out.println("--------------main end--------------");
    }

    public static Thread getSingletonThread() {
        return new Thread(new Runnable() {
            @Override
            public void run() {

                Thread t = Thread.currentThread();
                System.out.println("-------------" + t.getName() + ":start----------------------");
                LazySingletonV2 lazySingletonV2 = LazySingletonV2.getInstance();
                System.out.println(lazySingletonV2);
            }
        });
    }
}
