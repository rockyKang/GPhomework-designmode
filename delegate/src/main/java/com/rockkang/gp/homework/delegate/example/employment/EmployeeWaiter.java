package com.rockkang.gp.homework.delegate.example.employment;

public class EmployeeWaiter implements Employee {
    @Override
    public void doing(String food) {
        System.out.println("我是服务员,上菜:" + food);
    }
}
