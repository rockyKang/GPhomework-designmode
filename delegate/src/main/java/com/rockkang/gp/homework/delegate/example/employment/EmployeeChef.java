package com.rockkang.gp.homework.delegate.example.employment;

public class EmployeeChef implements Employee {

    @Override
    public void doing(String food) {
        System.out.println("我是大厨，开始炒菜:" + food);
    }
}
