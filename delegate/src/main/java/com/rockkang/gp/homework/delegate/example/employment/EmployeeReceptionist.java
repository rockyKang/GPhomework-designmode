package com.rockkang.gp.homework.delegate.example.employment;

import java.util.HashMap;
import java.util.Map;

public class EmployeeReceptionist implements Employee {

    private Map<String, Employee> target = new HashMap<>();

    public EmployeeReceptionist() {
        target.put("cook", new EmployeeChef());
        target.put("serving", new EmployeeWaiter());
    }

    @Override
    public void doing(String food) {
        System.out.println("我是招待收到您的点餐:" + food);
        target.get("cook").doing(food);
        target.get("serving").doing(food);
    }
}
