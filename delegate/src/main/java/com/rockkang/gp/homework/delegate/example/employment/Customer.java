package com.rockkang.gp.homework.delegate.example.employment;

public class Customer {
    public void order(String food, EmployeeReceptionist receptionist) {
        System.out.println("我要点餐:" + food);
        receptionist.doing(food);
    }
}
